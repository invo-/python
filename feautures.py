# Habrahabr @JRazor

#  Бесконечно вложенный список
print('Бесконечно вложенный список')
a = [ 1, 2, 3, 4 ]
a.append(a) # Добавляем элемент в конец списка

print(a) # Результат : [1, 2, 3, 4, [...]]
print( a[0] ) # Результат : 1
print( a[4] [4] [4] [4] [4] [4] [4] [4] [4] == a ) # Результат : 4

a = {}
b = {}
a['b'] = b
b['a'] = a
print(a) # Результат : {'b': {'a': {...}}}

# ---- // ---- # 
# Форматирование списка
print()
print('Форматирование списка')

l = [ [1, 2, 3], [4, 5], [7, 8, 9] ]
print( sum(l, []) ) # Результат : [1, 2, 3, 4, 5, 6, 7, 8, 9]

# ---- // ---- # 
# Генератор словарей 
print()
print('Генератор словарей')
a = { a:a**2 for a in range(1, 10) } 
print(a)


# ---- // ---- #
# Одноразовая функция в классе
class foo:
          def normal_call(self):
                    print("normal_call")
          def call(self):
                    print("first_call")
                    self.call = self.normal_call

# ---- // ---- #
# Получение аттрибутов класса
class GetAttr(object):
          def __getattribute__(self, name):
		f = lambda: "Hello {}".format(name)
		return f
g = GetAttr()
print( g.Mark() )
