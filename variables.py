f_var = 6
f_var -= 9
f_var += 3
print(f_var) # Result : 0

string = "Heyoo "
string += "world!"
print (string) # Result : "Heyoo world!"

string, f_var = f_var, string

print()
print( string, f_var ) # Result : "0 Heyoo world!"

s_var = 17, "string", "invo"
print(s_var) # Result : "(17, 'string', 'invo')"
