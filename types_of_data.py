# Список
#
# Список - это структура данных, которая содержит в себе
# упорядоченный набор элементов
#
list = [ 'phone', 'number', 'someone']
print(list) # ['phone', 'number', 'someone']


list.append('yellow')
print(list) # ['phone', 'number', 'someone', 'yellow']


list.sort()
print(list) # ['number', 'phone', 'someone', 'yellow']


item = list[0]
print(item) # number


del list[0]
print(list) # ['phone', 'someone', 'yellow']




# Кортеж
#
# Кортежи служат для хранения нескольких объектов вместе
# 
zoo = ('python', 'elephant', 'pinguin') # скобки не обязательны
print( 'Количество животных в зоопарке = ',len(zoo) ) 
print( zoo[0], zoo[2] ) # python pinguin
print( len(zoo) -1 + len(zoo[2]) ) # 9





# Словарь
#
# Словарь - это некий аналог адресной книги, 
# в которой можно найти адресат или контактную информацию о человеке,
# Зная лишь его имя
# 
# Заметьте, что ключ должен быть уникальным - вы ведь не сможете получить
# корректную информацию, если у вас записаны два человека с полностью одинаковыми именами
#
adr_book = {
          'Swaroop'   : 'swaroop@swaroopch.com',
          'Larry'     : 'larry@wall.org',
          'Matsumoto' : 'matz@ruby-lang.org',
          'Spammer'   : 'spammer@hotmail.com'
}
print(adr_book['Matsumoto']) # matz@ruby-lang.org

del adr_book['Spammer']
print(adr_book) # {'Matsumoto': 'matz@ruby-lang.org', 'Larry': 'larry@wall.org', 'Swaroop': 'swaroop@swaroopch.com'}

adr_book['Guido'] = 'guido@python.org'
print('Guido') # Guido





# Последовательности
# Списки, кортежи и строки являются примерами последовательностей.
#
# Основные возможности – это проверка принадлежности (т.е. выражения “in” и “not in”)
# и оператор индексирования, позволяющий получить напрямую некоторый элемент последовательности.
# 
shoplist = ['apple', 'food', 'www']
name = 'swaroop'

# Индексирование
print( 'Элемент 0 - ', shoplist[0] ) # Элемент 0 -  apple
print( 'Элемент 2 - ', shoplist[1] ) # Элемент 2 -  food

# Вырезка из списка
print( 'Элементы с 1 по 3 : ', shoplist[1:3] ) # Элементы с 1 по 3 :  ['food', 'www']
print( 'Элементы с 2 до конца : ', shoplist[2:] ) # Элементы с 2 до конца :  ['www']
print( 'Символы от начала до конца :  ', shoplist ) # Символы от начала до конца :   ['apple', 'food', 'www']





# Множество
#
# Множества - это неупорядоченные наборы простых объектов
#
# Они необходимы тогда, когда присутствие объекта в наборе важнее порядка
# или того, сколько раз данный объект там встречается
#
# Используя множества, можно осуществлять проверку принадлежности, определять, является ли
# данное множество подмножеством другого множества, находить пересечения множеств и т.д.
#
bri = set( ['Russia', 'England', 'India'] )
print( 'Russia' in bri ) # True
print( 'USA' in bri ) # False

bric = bri.copy()
bric.add('China')
print( bric.issuperset(bri) ) # True

bri.remove('England')
print( bri & bric ) # Или bri.intersection(bric) ; {'India', 'Russia'}





# Ссылки
#
# Когда мы создаём объект и присваеваем его переменной, переменная только ссылается
# на объект, а не представляет собой этот объект!
#
